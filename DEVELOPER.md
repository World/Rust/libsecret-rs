# Development

## Update bindings

Run the following commands:

```console
$ ./generator.py libsecret/sys/Gir.toml
$ ./generator.py libsecret/Gir.toml
```

## Create a release

Use [cargo release](https://github.com/crate-ci/cargo-release):

```console
$ cargo release --workspace minor --execute
```
