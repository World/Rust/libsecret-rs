# libsecret-rs

The Rust bindings of [libsecret](https://gitlab.gnome.org/GNOME/libsecret).

- Website: <https://gitlab.gnome.org/World/Rust/libsecret-rs/>

## Maintenance

This crate is in maintenace-only mode; we will keep it up to date with the gtk-core-rs crates, but
do not expect new development or bugfixes.  Please contribute merge requests instead.

## Documentation

- libsecret: <https://world.pages.gitlab.gnome.org/Rust/libsecret-rs/libsecret>
- libsecret_sys: <https://world.pages.gitlab.gnome.org/Rust/libsecret-rs/libsecret_sys>
