// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

mod collection;
pub use self::collection::Collection;

mod item;
pub use self::item::Item;

mod prompt;
pub use self::prompt::Prompt;

#[cfg(feature = "v0_19")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_19")))]
mod retrievable;
#[cfg(feature = "v0_19")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_19")))]
pub use self::retrievable::Retrievable;

mod service;
pub use self::service::Service;

mod schema;
pub use self::schema::Schema;

mod schema_attribute;
pub use self::schema_attribute::SchemaAttribute;

mod enums;
pub use self::enums::Error;
pub use self::enums::SchemaAttributeType;
#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
pub use self::enums::SchemaType;

mod flags;
pub use self::flags::CollectionCreateFlags;
pub use self::flags::CollectionFlags;
pub use self::flags::ItemCreateFlags;
pub use self::flags::ItemFlags;
pub use self::flags::SchemaFlags;
pub use self::flags::SearchFlags;
pub use self::flags::ServiceFlags;

pub(crate) mod functions;

mod constants;
pub use self::constants::COLLECTION_DEFAULT;
pub use self::constants::COLLECTION_SESSION;

pub(crate) mod traits {
    pub use super::collection::CollectionExt;
    pub use super::item::ItemExt;
    pub use super::prompt::PromptExt;
    #[cfg(feature = "v0_19")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v0_19")))]
    pub use super::retrievable::RetrievableExt;
    pub use super::service::ServiceExt;
}
