// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

#[cfg(feature = "v0_19")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_19")))]
use crate::Retrievable;
use crate::{ffi, ItemFlags, Service, Value};
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, pin::Pin};

#[cfg(feature = "v0_19")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_19")))]
glib::wrapper! {
    #[doc(alias = "SecretItem")]
    pub struct Item(Object<ffi::SecretItem, ffi::SecretItemClass>) @extends gio::DBusProxy, @implements gio::DBusInterface, gio::Initable, Retrievable;

    match fn {
        type_ => || ffi::secret_item_get_type(),
    }
}

#[cfg(not(any(feature = "v0_19")))]
glib::wrapper! {
    #[doc(alias = "SecretItem")]
    pub struct Item(Object<ffi::SecretItem, ffi::SecretItemClass>) @extends gio::DBusProxy, @implements gio::DBusInterface, gio::Initable;

    match fn {
        type_ => || ffi::secret_item_get_type(),
    }
}

impl Item {
    pub const NONE: Option<&'static Item> = None;

    #[doc(alias = "secret_item_new_for_dbus_path_sync")]
    #[doc(alias = "new_for_dbus_path_sync")]
    pub fn for_dbus_path_sync(
        service: Option<&impl IsA<Service>>,
        item_path: &str,
        flags: ItemFlags,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Item, glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let ret = ffi::secret_item_new_for_dbus_path_sync(
                service.map(|p| p.as_ref()).to_glib_none().0,
                item_path.to_glib_none().0,
                flags.into_glib(),
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "secret_item_new_for_dbus_path")]
    pub fn new_for_dbus_path<P: FnOnce(Result<Item, glib::Error>) + 'static>(
        service: Option<&impl IsA<Service>>,
        item_path: &str,
        flags: ItemFlags,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    ) {
        let main_context = glib::MainContext::ref_thread_default();
        let is_main_context_owner = main_context.is_owner();
        let has_acquired_main_context = (!is_main_context_owner)
            .then(|| main_context.acquire().ok())
            .flatten();
        assert!(
            is_main_context_owner || has_acquired_main_context.is_some(),
            "Async operations only allowed if the thread is owning the MainContext"
        );

        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> =
            Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn new_for_dbus_path_trampoline<
            P: FnOnce(Result<Item, glib::Error>) + 'static,
        >(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = std::ptr::null_mut();
            let ret = ffi::secret_item_new_for_dbus_path_finish(res, &mut error);
            let result = if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> =
                Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = new_for_dbus_path_trampoline::<P>;
        unsafe {
            ffi::secret_item_new_for_dbus_path(
                service.map(|p| p.as_ref()).to_glib_none().0,
                item_path.to_glib_none().0,
                flags.into_glib(),
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    pub fn new_for_dbus_path_future(
        service: Option<&(impl IsA<Service> + Clone + 'static)>,
        item_path: &str,
        flags: ItemFlags,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<Item, glib::Error>> + 'static>> {
        let service = service.map(ToOwned::to_owned);
        let item_path = String::from(item_path);
        Box_::pin(gio::GioFuture::new(&(), move |_obj, cancellable, send| {
            Self::new_for_dbus_path(
                service.as_ref().map(::std::borrow::Borrow::borrow),
                &item_path,
                flags,
                Some(cancellable),
                move |res| {
                    send.resolve(res);
                },
            );
        }))
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Item>> Sealed for T {}
}

pub trait ItemExt: IsA<Item> + sealed::Sealed + 'static {
    #[doc(alias = "secret_item_delete")]
    fn delete<P: FnOnce(Result<(), glib::Error>) + 'static>(
        &self,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    ) {
        let main_context = glib::MainContext::ref_thread_default();
        let is_main_context_owner = main_context.is_owner();
        let has_acquired_main_context = (!is_main_context_owner)
            .then(|| main_context.acquire().ok())
            .flatten();
        assert!(
            is_main_context_owner || has_acquired_main_context.is_some(),
            "Async operations only allowed if the thread is owning the MainContext"
        );

        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> =
            Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn delete_trampoline<P: FnOnce(Result<(), glib::Error>) + 'static>(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = std::ptr::null_mut();
            let _ = ffi::secret_item_delete_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> =
                Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = delete_trampoline::<P>;
        unsafe {
            ffi::secret_item_delete(
                self.as_ref().to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn delete_future(
        &self,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.delete(Some(cancellable), move |res| {
                send.resolve(res);
            });
        }))
    }

    #[doc(alias = "secret_item_delete_sync")]
    fn delete_sync(
        &self,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::secret_item_delete_sync(
                self.as_ref().to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[cfg(not(feature = "v0_19"))]
    #[cfg_attr(docsrs, doc(cfg(not(feature = "v0_19"))))]
    #[doc(alias = "secret_item_get_created")]
    #[doc(alias = "get_created")]
    fn created(&self) -> u64 {
        unsafe { ffi::secret_item_get_created(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "secret_item_get_flags")]
    #[doc(alias = "get_flags")]
    fn flags(&self) -> ItemFlags {
        unsafe { from_glib(ffi::secret_item_get_flags(self.as_ref().to_glib_none().0)) }
    }

    #[cfg(not(feature = "v0_19"))]
    #[cfg_attr(docsrs, doc(cfg(not(feature = "v0_19"))))]
    #[doc(alias = "secret_item_get_label")]
    #[doc(alias = "get_label")]
    fn label(&self) -> glib::GString {
        unsafe { from_glib_full(ffi::secret_item_get_label(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "secret_item_get_locked")]
    #[doc(alias = "get_locked")]
    #[doc(alias = "locked")]
    fn is_locked(&self) -> bool {
        unsafe { from_glib(ffi::secret_item_get_locked(self.as_ref().to_glib_none().0)) }
    }

    #[cfg(not(feature = "v0_19"))]
    #[cfg_attr(docsrs, doc(cfg(not(feature = "v0_19"))))]
    #[doc(alias = "secret_item_get_modified")]
    #[doc(alias = "get_modified")]
    fn modified(&self) -> u64 {
        unsafe { ffi::secret_item_get_modified(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "secret_item_get_schema_name")]
    #[doc(alias = "get_schema_name")]
    fn schema_name(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_full(ffi::secret_item_get_schema_name(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "secret_item_get_secret")]
    #[doc(alias = "get_secret")]
    fn secret(&self) -> Option<Value> {
        unsafe { from_glib_full(ffi::secret_item_get_secret(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "secret_item_get_service")]
    #[doc(alias = "get_service")]
    fn service(&self) -> Service {
        unsafe { from_glib_none(ffi::secret_item_get_service(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "secret_item_load_secret")]
    fn load_secret<P: FnOnce(Result<(), glib::Error>) + 'static>(
        &self,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    ) {
        let main_context = glib::MainContext::ref_thread_default();
        let is_main_context_owner = main_context.is_owner();
        let has_acquired_main_context = (!is_main_context_owner)
            .then(|| main_context.acquire().ok())
            .flatten();
        assert!(
            is_main_context_owner || has_acquired_main_context.is_some(),
            "Async operations only allowed if the thread is owning the MainContext"
        );

        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> =
            Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn load_secret_trampoline<
            P: FnOnce(Result<(), glib::Error>) + 'static,
        >(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = std::ptr::null_mut();
            let _ = ffi::secret_item_load_secret_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> =
                Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = load_secret_trampoline::<P>;
        unsafe {
            ffi::secret_item_load_secret(
                self.as_ref().to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn load_secret_future(
        &self,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.load_secret(Some(cancellable), move |res| {
                send.resolve(res);
            });
        }))
    }

    #[doc(alias = "secret_item_load_secret_sync")]
    fn load_secret_sync(
        &self,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::secret_item_load_secret_sync(
                self.as_ref().to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "secret_item_refresh")]
    fn refresh(&self) {
        unsafe {
            ffi::secret_item_refresh(self.as_ref().to_glib_none().0);
        }
    }

    #[doc(alias = "secret_item_set_label")]
    fn set_label<P: FnOnce(Result<(), glib::Error>) + 'static>(
        &self,
        label: &str,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    ) {
        let main_context = glib::MainContext::ref_thread_default();
        let is_main_context_owner = main_context.is_owner();
        let has_acquired_main_context = (!is_main_context_owner)
            .then(|| main_context.acquire().ok())
            .flatten();
        assert!(
            is_main_context_owner || has_acquired_main_context.is_some(),
            "Async operations only allowed if the thread is owning the MainContext"
        );

        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> =
            Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn set_label_trampoline<P: FnOnce(Result<(), glib::Error>) + 'static>(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = std::ptr::null_mut();
            let _ = ffi::secret_item_set_label_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> =
                Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = set_label_trampoline::<P>;
        unsafe {
            ffi::secret_item_set_label(
                self.as_ref().to_glib_none().0,
                label.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn set_label_future(
        &self,
        label: &str,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        let label = String::from(label);
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.set_label(&label, Some(cancellable), move |res| {
                send.resolve(res);
            });
        }))
    }

    #[doc(alias = "secret_item_set_label_sync")]
    fn set_label_sync(
        &self,
        label: &str,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::secret_item_set_label_sync(
                self.as_ref().to_glib_none().0,
                label.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "secret_item_set_secret")]
    fn set_secret<P: FnOnce(Result<(), glib::Error>) + 'static>(
        &self,
        value: &Value,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
        callback: P,
    ) {
        let main_context = glib::MainContext::ref_thread_default();
        let is_main_context_owner = main_context.is_owner();
        let has_acquired_main_context = (!is_main_context_owner)
            .then(|| main_context.acquire().ok())
            .flatten();
        assert!(
            is_main_context_owner || has_acquired_main_context.is_some(),
            "Async operations only allowed if the thread is owning the MainContext"
        );

        let user_data: Box_<glib::thread_guard::ThreadGuard<P>> =
            Box_::new(glib::thread_guard::ThreadGuard::new(callback));
        unsafe extern "C" fn set_secret_trampoline<P: FnOnce(Result<(), glib::Error>) + 'static>(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = std::ptr::null_mut();
            let _ = ffi::secret_item_set_secret_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<glib::thread_guard::ThreadGuard<P>> =
                Box_::from_raw(user_data as *mut _);
            let callback: P = callback.into_inner();
            callback(result);
        }
        let callback = set_secret_trampoline::<P>;
        unsafe {
            ffi::secret_item_set_secret(
                self.as_ref().to_glib_none().0,
                value.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn set_secret_future(
        &self,
        value: &Value,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        let value = value.clone();
        Box_::pin(gio::GioFuture::new(self, move |obj, cancellable, send| {
            obj.set_secret(&value, Some(cancellable), move |res| {
                send.resolve(res);
            });
        }))
    }

    #[doc(alias = "secret_item_set_secret_sync")]
    fn set_secret_sync(
        &self,
        value: &Value,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::secret_item_set_secret_sync(
                self.as_ref().to_glib_none().0,
                value.to_glib_none().0,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "locked")]
    fn connect_locked_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_locked_trampoline<P: IsA<Item>, F: Fn(&P) + 'static>(
            this: *mut ffi::SecretItem,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(Item::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::locked\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_locked_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<Item>> ItemExt for O {}
