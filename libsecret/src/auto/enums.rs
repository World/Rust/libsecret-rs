// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::ffi;
use glib::{prelude::*, translate::*};

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "SecretError")]
pub enum Error {
    #[doc(alias = "SECRET_ERROR_PROTOCOL")]
    Protocol,
    #[doc(alias = "SECRET_ERROR_IS_LOCKED")]
    IsLocked,
    #[doc(alias = "SECRET_ERROR_NO_SUCH_OBJECT")]
    NoSuchObject,
    #[doc(alias = "SECRET_ERROR_ALREADY_EXISTS")]
    AlreadyExists,
    #[doc(alias = "SECRET_ERROR_INVALID_FILE_FORMAT")]
    InvalidFileFormat,
    #[cfg(feature = "v0_21_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v0_21_2")))]
    #[doc(alias = "SECRET_ERROR_MISMATCHED_SCHEMA")]
    MismatchedSchema,
    #[cfg(feature = "v0_21_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v0_21_2")))]
    #[doc(alias = "SECRET_ERROR_NO_MATCHING_ATTRIBUTE")]
    NoMatchingAttribute,
    #[cfg(feature = "v0_21_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v0_21_2")))]
    #[doc(alias = "SECRET_ERROR_WRONG_TYPE")]
    WrongType,
    #[cfg(feature = "v0_21_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v0_21_2")))]
    #[doc(alias = "SECRET_ERROR_EMPTY_TABLE")]
    EmptyTable,
    #[doc(hidden)]
    __Unknown(i32),
}

#[doc(hidden)]
impl IntoGlib for Error {
    type GlibType = ffi::SecretError;

    #[inline]
    fn into_glib(self) -> ffi::SecretError {
        match self {
            Self::Protocol => ffi::SECRET_ERROR_PROTOCOL,
            Self::IsLocked => ffi::SECRET_ERROR_IS_LOCKED,
            Self::NoSuchObject => ffi::SECRET_ERROR_NO_SUCH_OBJECT,
            Self::AlreadyExists => ffi::SECRET_ERROR_ALREADY_EXISTS,
            Self::InvalidFileFormat => ffi::SECRET_ERROR_INVALID_FILE_FORMAT,
            #[cfg(feature = "v0_21_2")]
            Self::MismatchedSchema => ffi::SECRET_ERROR_MISMATCHED_SCHEMA,
            #[cfg(feature = "v0_21_2")]
            Self::NoMatchingAttribute => ffi::SECRET_ERROR_NO_MATCHING_ATTRIBUTE,
            #[cfg(feature = "v0_21_2")]
            Self::WrongType => ffi::SECRET_ERROR_WRONG_TYPE,
            #[cfg(feature = "v0_21_2")]
            Self::EmptyTable => ffi::SECRET_ERROR_EMPTY_TABLE,
            Self::__Unknown(value) => value,
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::SecretError> for Error {
    #[inline]
    unsafe fn from_glib(value: ffi::SecretError) -> Self {
        match value {
            ffi::SECRET_ERROR_PROTOCOL => Self::Protocol,
            ffi::SECRET_ERROR_IS_LOCKED => Self::IsLocked,
            ffi::SECRET_ERROR_NO_SUCH_OBJECT => Self::NoSuchObject,
            ffi::SECRET_ERROR_ALREADY_EXISTS => Self::AlreadyExists,
            ffi::SECRET_ERROR_INVALID_FILE_FORMAT => Self::InvalidFileFormat,
            #[cfg(feature = "v0_21_2")]
            ffi::SECRET_ERROR_MISMATCHED_SCHEMA => Self::MismatchedSchema,
            #[cfg(feature = "v0_21_2")]
            ffi::SECRET_ERROR_NO_MATCHING_ATTRIBUTE => Self::NoMatchingAttribute,
            #[cfg(feature = "v0_21_2")]
            ffi::SECRET_ERROR_WRONG_TYPE => Self::WrongType,
            #[cfg(feature = "v0_21_2")]
            ffi::SECRET_ERROR_EMPTY_TABLE => Self::EmptyTable,
            value => Self::__Unknown(value),
        }
    }
}

impl StaticType for Error {
    #[inline]
    #[doc(alias = "secret_error_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::secret_error_get_type()) }
    }
}

impl glib::HasParamSpec for Error {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

impl glib::value::ValueType for Error {
    type Type = Self;
}

unsafe impl<'a> glib::value::FromValue<'a> for Error {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl ToValue for Error {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

impl From<Error> for glib::Value {
    #[inline]
    fn from(v: Error) -> Self {
        ToValue::to_value(&v)
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "SecretSchemaAttributeType")]
pub enum SchemaAttributeType {
    #[doc(alias = "SECRET_SCHEMA_ATTRIBUTE_STRING")]
    String,
    #[doc(alias = "SECRET_SCHEMA_ATTRIBUTE_INTEGER")]
    Integer,
    #[doc(alias = "SECRET_SCHEMA_ATTRIBUTE_BOOLEAN")]
    Boolean,
    #[doc(hidden)]
    __Unknown(i32),
}

#[doc(hidden)]
impl IntoGlib for SchemaAttributeType {
    type GlibType = ffi::SecretSchemaAttributeType;

    #[inline]
    fn into_glib(self) -> ffi::SecretSchemaAttributeType {
        match self {
            Self::String => ffi::SECRET_SCHEMA_ATTRIBUTE_STRING,
            Self::Integer => ffi::SECRET_SCHEMA_ATTRIBUTE_INTEGER,
            Self::Boolean => ffi::SECRET_SCHEMA_ATTRIBUTE_BOOLEAN,
            Self::__Unknown(value) => value,
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::SecretSchemaAttributeType> for SchemaAttributeType {
    #[inline]
    unsafe fn from_glib(value: ffi::SecretSchemaAttributeType) -> Self {
        match value {
            ffi::SECRET_SCHEMA_ATTRIBUTE_STRING => Self::String,
            ffi::SECRET_SCHEMA_ATTRIBUTE_INTEGER => Self::Integer,
            ffi::SECRET_SCHEMA_ATTRIBUTE_BOOLEAN => Self::Boolean,
            value => Self::__Unknown(value),
        }
    }
}

impl StaticType for SchemaAttributeType {
    #[inline]
    #[doc(alias = "secret_schema_attribute_type_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::secret_schema_attribute_type_get_type()) }
    }
}

impl glib::HasParamSpec for SchemaAttributeType {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

impl glib::value::ValueType for SchemaAttributeType {
    type Type = Self;
}

unsafe impl<'a> glib::value::FromValue<'a> for SchemaAttributeType {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl ToValue for SchemaAttributeType {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

impl From<SchemaAttributeType> for glib::Value {
    #[inline]
    fn from(v: SchemaAttributeType) -> Self {
        ToValue::to_value(&v)
    }
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "SecretSchemaType")]
pub enum SchemaType {
    #[doc(alias = "SECRET_SCHEMA_TYPE_NOTE")]
    Note,
    #[doc(alias = "SECRET_SCHEMA_TYPE_COMPAT_NETWORK")]
    CompatNetwork,
    #[doc(hidden)]
    __Unknown(i32),
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
#[doc(hidden)]
impl IntoGlib for SchemaType {
    type GlibType = ffi::SecretSchemaType;

    #[inline]
    fn into_glib(self) -> ffi::SecretSchemaType {
        match self {
            Self::Note => ffi::SECRET_SCHEMA_TYPE_NOTE,
            Self::CompatNetwork => ffi::SECRET_SCHEMA_TYPE_COMPAT_NETWORK,
            Self::__Unknown(value) => value,
        }
    }
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
#[doc(hidden)]
impl FromGlib<ffi::SecretSchemaType> for SchemaType {
    #[inline]
    unsafe fn from_glib(value: ffi::SecretSchemaType) -> Self {
        match value {
            ffi::SECRET_SCHEMA_TYPE_NOTE => Self::Note,
            ffi::SECRET_SCHEMA_TYPE_COMPAT_NETWORK => Self::CompatNetwork,
            value => Self::__Unknown(value),
        }
    }
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
impl StaticType for SchemaType {
    #[inline]
    #[doc(alias = "secret_schema_type_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::secret_schema_type_get_type()) }
    }
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
impl glib::HasParamSpec for SchemaType {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
impl glib::value::ValueType for SchemaType {
    type Type = Self;
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
unsafe impl<'a> glib::value::FromValue<'a> for SchemaType {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
impl ToValue for SchemaType {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
impl From<SchemaType> for glib::Value {
    #[inline]
    fn from(v: SchemaType) -> Self {
        ToValue::to_value(&v)
    }
}
