// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::ffi;
#[cfg(feature = "v0_19")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_19")))]
use crate::{Retrievable, Value};
#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
use crate::{Schema, SchemaType};
use glib::{prelude::*, translate::*};

//#[cfg(feature = "v0_21_2")]
//#[cfg_attr(docsrs, doc(cfg(feature = "v0_21_2")))]
//#[doc(alias = "secret_attributes_validate")]
//pub fn attributes_validate(schema: &Schema, attributes: /*Unknown conversion*//*Unimplemented*/HashTable TypeId { ns_id: 0, id: 25 }/TypeId { ns_id: 0, id: 25 }) -> Result<(), glib::Error> {
//    unsafe { TODO: call ffi:secret_attributes_validate() }
//}

#[cfg(feature = "v0_18_6")]
#[cfg_attr(docsrs, doc(cfg(feature = "v0_18_6")))]
#[doc(alias = "secret_get_schema")]
#[doc(alias = "get_schema")]
pub fn schema(type_: SchemaType) -> Schema {
    unsafe { from_glib_none(ffi::secret_get_schema(type_.into_glib())) }
}

#[doc(alias = "secret_password_free")]
pub fn password_free(password: Option<&str>) {
    unsafe {
        ffi::secret_password_free(password.to_glib_none().0);
    }
}

#[doc(alias = "secret_password_wipe")]
pub fn password_wipe(password: Option<&str>) {
    unsafe {
        ffi::secret_password_wipe(password.to_glib_none().0);
    }
}
